# textdomain: frame
The frame contains an item the player can not obtain, therefore remains locked.=
Frame is now unlocked.=
Frame is now locked.=
This item can not be inserted in a frame.=
Item inserted into frame is not obtainable, frame locked.=
Frame with @1=
Frame=
