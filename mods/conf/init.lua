
conf = {}

conf.boxes = {}

conf.boxes.entry_lobby = tonumber(minetest.settings:get("boxes_entry_lobby") or -1)
conf.boxes.exit_lobby = tonumber(minetest.settings:get("boxes_exit_lobby") or -2)
conf.boxes.first_box = tonumber(minetest.settings:get("boxes_first_box") or 0)

conf.tutorial = {}

conf.tutorial.required = (minetest.settings:get("tutorial_required") or "true") == "true"
conf.tutorial.series = tonumber(minetest.settings:get("tutorial_series") or "1")
conf.tutorial.entry_lobby = tonumber(minetest.settings:get("tutorial_entry_lobby") or -3)
conf.tutorial.exit_lobby = tonumber(minetest.settings:get("tutorial_exit_lobby") or -4)
conf.tutorial.exit = minetest.string_to_pos(minetest.settings:get("tutorial_exit") or "(0,0,0)")

conf.lobby = {}

conf.lobby.respawn_center = minetest.string_to_pos(minetest.settings:get("lobby_respawn_center") or "(0,0,0)")
conf.lobby.respawn_radius = minetest.settings:get("lobby_respawn_radius") or "8,12"

conf.telex = {}
conf.telex.enable = minetest.settings:get_bool("telex_enable", false)
