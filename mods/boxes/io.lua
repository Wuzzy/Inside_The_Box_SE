
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

local S = minetest.get_translator("boxes")

local function bytes_to_string(bytes)
	local s = {}
	for i = 1, #bytes do
		s[i] = string.char(bytes[i])
	end
	return table.concat(s)
end

function boxes.import_box(id, box_data)
	local data_index = 1
	local function read_u8()
		local c = string.byte(box_data, data_index)
		data_index = data_index + 1
		return c
	end
	local function read_u16()
		local a = read_u8()
		local b = read_u8()
		return 256 * a + b
	end
	local function read_pos()
		local x = read_u16()
		local y = read_u16()
		local z = read_u16()
		return {x = x, y = y, z = z}
	end
	local version = read_u8()
	assert (version == 1)
	local type = read_u8()
	local size = read_pos()
	local entry = read_pos()
	local exit = read_pos()
	db.box_set_data(id, string.sub(box_data, data_index))
	db.box_set_meta(id, {
		type = type,
		meta = {
			size = size,
			entry = entry,
			exit = exit,
		}
	})
end

function boxes.export_box(id)
	local data_index = 1
	local data = {}
	local function write_u8(x)
		data[data_index] = x
		data_index = data_index + 1
	end
	local function write_u16(x)
		write_u8(math.floor(x / 256))
		write_u8(x % 256)
	end
	local function write_pos(p)
		write_u16(p.x)
		write_u16(p.y)
		write_u16(p.z)
	end
	local meta = db.box_get_meta(id)
	write_u8(1) -- version
	write_u8(meta.type)
	write_pos(meta.meta.size)
	write_pos(meta.meta.entry)
	write_pos(meta.meta.exit)
	return bytes_to_string(data) .. db.box_get_data(id)
end

local function read_file(filename)
	local file = io.open(filename, "r")
	if file ~= nil then
		local file_content = file:read("*all")
		io.close(file)
		return file_content
	end
	return ""
end

local function write_file(filename, data)
	local file, err = io.open(filename, "w")
	if file then
		file:write(data)
		io.close(file)
		return true
	else
		error(err)
		return false
	end
end

local modpath = minetest.get_modpath(minetest.get_current_modname())

if not db.box_exists(conf.boxes.entry_lobby) then
	local box_data = read_file(modpath .. "/entry.box")
	boxes.import_box(conf.boxes.entry_lobby, box_data)
end

if not db.box_exists(conf.boxes.exit_lobby) then
	local box_data = read_file(modpath .. "/exit.box")
	boxes.import_box(conf.boxes.exit_lobby, box_data)
end

if not db.box_exists(conf.tutorial.entry_lobby) then
	local box_data = read_file(modpath .. "/tutorial_entry.box")
	boxes.import_box(conf.tutorial.entry_lobby, box_data)
end

if not db.box_exists(conf.tutorial.exit_lobby) then
	local box_data = read_file(modpath .. "/tutorial_exit.box")
	boxes.import_box(conf.tutorial.exit_lobby, box_data)
end

if not db.box_exists(conf.boxes.first_box) then
	local box_data = read_file(modpath .. "/box.box")
	boxes.import_box(conf.boxes.first_box, box_data)
end

-- local worldpath = minetest.get_worldpath()
-- write_file(worldpath .. "/entry.box", boxes.export_box(conf.boxes.entry_lobby))
-- write_file(worldpath .. "/exit.box", boxes.export_box(conf.boxes.exit_lobby))
-- write_file(worldpath .. "/box.box", boxes.export_box(conf.boxes.first_box))

minetest.register_chatcommand("export", {
	params = S("<box_id> | all"),
	description = S("Export a box (or all boxes) to '.box' file"),
	privs = {server = true},
	func = function(name, param)
		if param == "all" then
			local exported_boxes = {}
			local failed_boxes = {}
			local worldpath = minetest.get_worldpath()
			for box=0, db.get_last_box_id() do
				if db.box_exists(box) then
					local filename = box..".box"
					local ok = write_file(worldpath.."/"..filename, boxes.export_box(box))
					if not ok then
						table.insert(failed_boxs, box)
					else
						boxes.export_box(box)
						table.insert(exported_boxes, box)
					end
				end
			end
			local export = table.concat(exported_boxes, ", ")
			local failures = table.concat(failed_boxes, ", ")
			if #failures > 0 and #export > 0 then
				return true, S("Failed to export boxes @1. Exported boxes @2 to @3.", failures, export, worldpath)
			elseif #failures > 0 and #export == 0 then
				return true, S("Failed to export boxes @1.", failures)
			elseif #export > 0 then
				return true, S("Exported boxes @1 to @2.", export, worldpath)
			else
				return true, S("Nothing to export.")
			end
		end
		local box = tonumber(param)
		if not box then
			return false, S("Invalid box ID.")
		end
		if not db.box_exists(box) then
			return false, S("Box does not exist.")
		end
		local worldpath = minetest.get_worldpath()
		local filename = box..".box"
		local ok = write_file(worldpath .. "/" .. filename, boxes.export_box(box))
		if ok then
			return true, S("Box @1 exported to @2.", param, worldpath.."/"..filename)
		else
			return false, S("Error writing file.")
		end
	end,
})

minetest.register_chatcommand("import", {
	params = S("<box_id>"),
	description = S("Import a box from '.box' file"),
	privs = {server = true},
	func = function(name, param)
		local box = tonumber(param)
		if not box then
			return false, S("Invalid box ID.")
		end
		if not db.box_exists(box) then
			return false, S("Box does not exist.")
		end
		local worldpath = minetest.get_worldpath()
		local filename = box..".box"
		local box_data = read_file(worldpath.."/"..filename)
		boxes.import_box(box, box_data)
		return true, S("Box @1 imported from @2.", param, worldpath.."/"..filename)
	end,
})
