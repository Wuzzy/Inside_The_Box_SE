--
-- maps event tracks to actual music track names
--

music.set_tracks({
	join = "music_tracks_join",
	box = "music_tracks_box",
	create = "music_tracks_create",
	exit = "music_tracks_exit",
})
