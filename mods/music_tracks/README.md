# `music_tracks`
This is a mod for Inside the Box.
It adds some nice atmospheric music by The Cynic Project.

## Track list and credits
All music tracks are from the Pixelsphere soundtrack by The Cynic Project.

* `music_tracks_box.1.ogg`: Jazz V-VI-VII
* `music_tracks_box.2.ogg`: Vaporware
* `music_tracks_box.3.ogg`: Sirens in Darkness
* `music_tracks_box.4.ogg`: Absinthe Dreams
* `music_tracks_box.5.ogg`: A Happy Thought, Slowed Down
* `music_tracks_create.1.ogg`: Synthwave 3k
* `music_tracks_exit.1.ogg`: The Void
* `music_tracks_join.1.ogg`: Synthwave 4k

License: CC0 <https://creativecommons.org/publicdomain/zero/1.0/>

Evidence of permission:
Quote from <https://opengameart.org/users/cynicmusic> (as of 28 Jan 2020):
"My game [Pixelsphere] and 4 hour soundtrack are 100% free! (soundtrack, game,
streaming & web). It took me 5 years to make the Pixelsphere game &
assets (including 1000 SFX and 4 hours of music) and now I'm making
them available for you CC0 Public Domain to use in your games."
