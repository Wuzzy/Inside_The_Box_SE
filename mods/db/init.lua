--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

--[[

  Mod storage backend for insidethebox

  This backend stores various persistent world data bits that are
  not properly stored by the Luanti engine. This mod provides
  persistent but not neccessarily performant data storage for the
  itb game until a better solution exists.

  Major data storage parts in this db:

  - players, player scores
    No player metadata storage exists currently. Only HP and air
    are stored in player data. We lack storage for things like
    skin choice, stamina, RPG points and skills, etc.

  - box data

--]]

-- gratuitous banner
minetest.log("action", "         _|_|_|                      _|        _|")
minetest.log("action", "           _|    _|_|_|      _|_|_|        _|_|_|    _|_|")
minetest.log("action", "           _|    _|    _|  _|_|      _|  _|    _|  _|_|_|_|")
minetest.log("action", "           _|    _|    _|      _|_|  _|  _|    _|  _|")
minetest.log("action", "         _|_|_|  _|    _|  _|_|_|    _|    _|_|_|    _|_|_|")
minetest.log("action", "")
minetest.log("action", "   _|      _|                          _|")
minetest.log("action", " _|_|_|_|  _|_|_|      _|_|            _|_|_|      _|_|    _|    _|")
minetest.log("action", "   _|      _|    _|  _|_|_|_|          _|    _|  _|    _|    _|_|")
minetest.log("action", "   _|      _|    _|  _|                _|    _|  _|    _|  _|    _|")
minetest.log("action", "     _|_|  _|    _|    _|_|_|          _|_|_|      _|_|    _|    _|")

-- mod storage structure and content
--[[
  string fields:

  itb_box: Contain the storage of the box content (not metadata) so all blocks
    - id: box identifier
    - data: box contents (nodes)

  itb_series: Contains existing series
    - id: series ID
    - name: name of the series
    - meta: misc data

  itb_player: player info
    - id: player number
    - name: player name
    - meta: random player data bits

  Number fields:
  - itb_max_player_id: Maximum player ID
  - itb_max_box_id: Maximum box ID

--]]

db = {}

local storage = minetest.get_mod_storage()
local last_box_id = -1

local decode = function(key)
	local str = storage:get_string(key)
	if str == "" then
		return {}
	else
		return minetest.deserialize(minetest.decode_base64(str)) or {}
	end
end
local encode = function(key, value)
	local coded = minetest.encode_base64(minetest.serialize(value))
	storage:set_string(key, coded)
end

function db.player_get_id(name)
	minetest.log("action", "db.player_get_id("..name..")")
	local players = decode("itb_player")
	local max_player_id = storage:get_int("itb_max_player_id")
	if players then
		for id, playerinfo in pairs(players) do
			if playerinfo.name == name then
				return id
			end
		end

		-- New player ID
		local id = max_player_id
		players[id] = {}
		players[id].name = name
		storage:set_int("itb_max_player_id", max_player_id)
		encode("itb_player", players)
		minetest.log("action", name .. " inserted successfully as player ID " .. id)
		max_player_id = max_player_id + 1
		return id
	end

	minetest.log("error", "db.player_get_id(" .. name .. "): No players found in storage")
	return nil
end

function db.player_get_name(player_id)
	minetest.log("action", "db.player_get_name("..player_id..")")
	local players = decode("itb_player")
	if players[player_id] then
		return players[player_id].name
	else
		return nil
	end
end

function db.player_points(player_id, box_id, type, values, limit)
	minetest.log("action", "db.player_player_points("..player_id..", ...)")
	local points = decode("itb_points")
	if limit > 0 then
		points[player_id] = {}
		points[player_id][box_id] = {}
		points[player_id][box_id][type] = {}
		-- add new entries
		local i, v = next(values, nil)
		while i and i <= limit do
			table.insert(points[player_id][box_id][type], v)
			i, v = next(values, i)
		end
	else
		local _, score = next(values)
		if not points[player_id] then
			points[player_id] = {}
		end
		if not points[player_id][box_id] then
			points[player_id][box_id] = {}
		end
		if not points[player_id][box_id][type] then
			points[player_id][box_id][type] = {}
		end
		table.insert(points[player_id][box_id][type], score)
	end
	encode("itb_points", points)
	return true
end

function db.player_get_completed_boxes(player_id)
	minetest.log("action", "db.player_get_completed_boxes("..player_id..")")
	local points = decode("itb_points")
	if not points[player_id] then
		return {}
	end
	local boxes = {}
	for box_id, _ in pairs(points[player_id]) do
		table.insert(boxes, box_id)
	end
	return boxes
end

-- This is a stub.
function db.player_get_points(filtertbl)
	minetest.log("action", "db.player_get_points()")
	return {}
end

function db.player_get_meta(name)
	minetest.log("action", "db.player_get_meta("..name..")")
	local players = decode("itb_player")

	-- trigger generation of ID record
	local player_id = db.player_get_id(name)
	if not player_id then
		minetest.log("error", "db.player_get_meta(" .. name .. "): no such player in db, unable to insert")
		return {series_progress = {}}
	end
	players = decode("itb_player")
	local player = players[player_id]
	if not player then
		minetest.log("error", "db.player_get_meta(" .. name .. "): no such player")
		return {series_progress = {}}
	end
	local outmeta
	if player.meta then
		outmeta = player.meta
	else
		outmeta = {}
	end
	if not outmeta.series_progress then
		outmeta.series_progress = {}
	end
	return outmeta
end

function db.player_set_meta(name, meta)
	minetest.log("action", "db.player_set_meta("..name..", <meta>)")

	local players = decode("itb_player")

	-- trigger generation of ID record
	local player_id = db.player_get_id(name)
	if not player_id then
		minetest.log("error", "db.player_set_meta(" .. name .. ", <meta>): no such player in db, unable to insert")
		return nil
	end

	-- Get players table again since the player might be new in DB
	-- and player_get_id updated the itb_player list
	players = decode("itb_player")
	local player = players[player_id]
	if not player then
		minetest.log("error", "db.player_set_meta(" .. name .. ", <meta>): no such player")
		return nil
	end
	player.meta = meta
	encode("itb_player", players)
	return true
end

function db.player_get_players()
	minetest.log("action", "db.player_get_players()")
	local players = decode("itb_player")
	if not players then
		return
	end
	local players_count = 1
	local players_out = {}
	for id, playerdata in pairs(players) do
		players_out[players_count] = {player_id = id, name = playerdata.name}
		players_count = players_count + 1
	end
	return players_out
end

function db.player_get_series_boxes(player_id, series_id)
	minetest.log("action", "db.get_series_boxes("..player_id..", "..series_id..")")
	local points = decode("itb_points")
	if not points or not points[player_id] then
		return {}
	end
	local boxes = {}
	for box_id, _ in pairs(points[player_id]) do
		table.insert(boxes, box_id)
	end
	return boxes
end

function db.series_create(name)
	minetest.log("action", "db.series_create("..name..")")
	assert(name ~= "")

	local series = decode("itb_series")
	table.insert(series, {
		name = name,
		boxes = {},
		meta = {
			meta = {}
		}
	})
	encode("itb_series", series)

	return #series
end

function db.series_destroy(series_id)
	minetest.log("action", "db.series_destroy("..series_id..")")
	assert(series_id)

	local series = decode("itb_series")
	if not series[series_id] then
		-- Series doesn't exist
		return false
	end
	series[series_id] = nil
	encode("itb_series", series)
	return true
end

function db.series_get_series()
	minetest.log("action", "db.get_series()")
	local series = decode("itb_series")
	local series_out = {}
	local series_count = 1
	for k,v in pairs(series) do
		series_out[series_count] = {id = k, name = v.name}
		series_count = series_count + 1
	end

	return series_out
end

db.SEQUENTIAL_TYPE = 0
db.RANDOM_ACCESS_TYPE = 1

function db.series_exists(series_id)
	minetest.log("action", "db.series_exists("..series_id..")")
	local series = decode("itb_series")
	return series[series_id] ~= nil
end

function db.series_get_meta(series_id)
	minetest.log("action", "db.series_get_meta("..series_id..")")
	local series = decode("itb_series")
	local serie = series[series_id]
	if not serie then
		minetest.log("warning", "db.series_get_meta(" .. series_id .. "): no such series")
		return nil
	end
	local result = {
		name = serie.name,
		meta = serie.meta or {},
	}
	if not result.meta.type then
		result.meta.type = db.SEQUENTIAL_TYPE
	end
	return result
end

function db.series_set_meta(series_id, meta)
	minetest.log("action", "db.series_set_meta("..series_id..", <meta>)")
	local series = decode("itb_series")
	local serie = series[series_id]
	if not serie then
		minetest.log("warning", "db.series_set_meta(" .. series_id .. "): no such series")
		return false
	end
	serie.name = meta.name
	serie.meta = meta.meta
	encode("itb_series", series)
	return true
end

function db.series_get_boxes(series_id)
	minetest.log("action", "db.series_get_boxes("..series_id..")")
	local series = decode("itb_series")
	local serie = series[series_id]
	if not serie then
		return {}
	end
	local boxes = {}
	local boxes_index = 1
	if not serie.boxes then
		return {}
	end
	for k,v in pairs(serie.boxes) do
		boxes[boxes_index] = v.box_id
		boxes_index = boxes_index + 1
	end
	return boxes
end

function db.series_insert_box(series_id, box_id, box_order)
	minetest.log("action", "db.series_insert_box("..series_id..", "..box_id..", "..tostring(box_order)..")")
	local series = decode("itb_series")
	local serie = series[series_id]
	if not serie then
		minetest.log("warning", "db.series_get_boxes(" .. series_id .. "): no such series")
		return nil
	end

	if not serie.boxes then
		serie.boxes = {}
	end
	if box_order then
		-- Add at provided index
		table.insert(serie.boxes, box_order, { box_id = box_id })
	else
		-- Add at end
		table.insert(serie.boxes, { box_id = box_id })
	end
	last_box_id = math.max(last_box_id, box_id)
	encode("itb_series", series)
	storage:set_int("itb_last_box_id", last_box_id)
	return true
end

function db.series_add_at_end(series_id, box_id)
	minetest.log("action", "db.series_add_at_end("..series_id..", "..box_id)
	return db.series_insert_box(series_id, box_id)
end

function db.series_delete_box(series_id, box_id)
	minetest.log("action", "db.series_delete_box("..series_id..", "..box_id)
	local series = decode("itb_series")
	local serie = series[series_id]
	if not serie then
		minetest.log("error", "db.series_delete_box(" .. series_id .. ", " .. box_id .. "): no such series")
		return nil
	end

	for k,v in pairs(serie.boxes) do
		if v and v.box_id == box_id then
			serie.boxes[k] = nil
			encode("itb_series", series)
			return true
		end
	end
	return false
end

function db.box_get_data(box_id)
	minetest.log("action", "db.box_get_data("..box_id..")")
	local boxes = decode("itb_box")
	if not boxes then
		minetest.log("error", "db.box_get_data(" .. box_id .."): no box data")
		return nil
	end

	if not boxes[box_id] then
		-- Initialize box data if not existing
		boxes[box_id] = {}
	end

	return boxes[box_id].data
end

function db.box_set_data(box_id, data)
	minetest.log("action", "db.box_set_data("..box_id..", <data>)")
	local boxes = decode("itb_box")
	if not boxes then
		minetest.log("error", "db.box_set_data(" .. box_id ..", <data>): can't access box data")
		return nil
	end

	if not boxes[box_id] then
		-- Initialize box data if not existing
		boxes[box_id] = {}
	end
	boxes[box_id].data = data
	encode("itb_box", boxes)
	return true
end

db.BOX_TYPE = 0
db.ENTRY_TYPE = 1
db.EXIT_TYPE = 2

db.STATUS_EDITING = 0
db.STATUS_SUBMITTED = 1
db.STATUS_ACCEPTED = 2

function db.get_last_box_id()
	minetest.log("action", "db.get_last_box_id()")
	if last_box_id == -1 then
		last_box_id = storage:get_int("itb_last_box_id") or -1
	end
	return last_box_id
end

function db.box_exists(box_id)
	minetest.log("action", "db.box_exists("..box_id..")")
	local boxes = decode("itb_box")
	if not boxes then
		minetest.log("error", "db.box_get_data(" .. box_id .."): no box data")
		return nil
	end

	return boxes[box_id] ~= nil
end

function db.box_get_meta(box_id)
	minetest.log("action", "db.box_get_meta("..box_id..")")

	local boxes = decode("itb_box")
	if not boxes then
		minetest.log("error", "db.box_get_meta(" .. box_id .."): no boxes found")
		return nil
	end
	local box = boxes[box_id]
	if not box then
		minetest.log("error", "db.box_get_meta(" .. box_id .."): no box")
		return nil
	end

	-- Get box meta
	local result = {
		type = box.type,
		meta = box.meta,
	}

	-- Add variou fallback values
	if not result.meta.status then
		result.meta.status = db.STATUS_EDITING
	end
	if result.type == db.BOX_TYPE and not result.meta.num_items then
		result.meta.num_items = 1
	end
	if result.type == db.BOX_TYPE and (not result.meta.time) then
		result.meta.num_completed_players = 0
		result.meta.time = {
			overall_best = 1e100,
			average_first = 0,
			average_best = 0,
		}
		result.meta.deaths = {
			overall_best = 1e100,
			average_first = 0,
			average_best = 0,
		}
		result.meta.damage = {
			overall_best = 1e100,
			average_first = 0,
			average_best = 0,
		}
	end
	if result.type == db.BOX_TYPE and not result.meta.box_name then
		result.meta.box_name = "(No name)"
	end
	if result.type == db.BOX_TYPE and not result.meta.skybox then
		result.meta.skybox = 0
	end
	if result.type == db.BOX_TYPE and not result.meta.build_time then
		result.meta.builder = ""
		result.meta.build_time = 0
	end
	if result.type == db.EXIT_TYPE and not result.meta.signs_to_update then
		result.meta.signs_to_update = {}
	end
	if result.type == db.EXIT_TYPE and not result.meta.star_positions then
		result.meta.star_positions = {}
	end
	if result.type == db.EXIT_TYPE and not result.meta.category_positions then
		result.meta.category_positions = {}
	end
	return result
end

-- TODO: Stub
function db.box_get_num_completed_players(box_id)
	minetest.log("action", "db.box_get_num_completed_players("..box_id..")")
	return 0
end

function db.box_set_meta(box_id, meta)
	minetest.log("action", "db.box_set_meta("..box_id..", <meta>)")
	local boxes = decode("itb_box")
	if not boxes then
		return false
	end
	if not boxes[box_id] then
		boxes[box_id] = {}
	end
	boxes[box_id].type = meta.type
	boxes[box_id].meta = meta.meta
	encode("itb_box", boxes)
	last_box_id = math.max(last_box_id, box_id)
	storage:set_int("itb_last_box_id", last_box_id)
	return true
end

function db.series_get_boxes_not_in(series_id)
	minetest.log("action", "db.series_get_boxes_not_in("..tostring(series_id)..")")

	local boxes = decode("itb_box")
	if not boxes then
		minetest.log("error", "db.series_get_boxes_not_in(" .. tostring(series_id) .."): no boxes found")
		return nil
	end
	local series = decode("itb_series")
	if not series then
		minetest.log("error", "db.series_get_boxes_not_in(" .. tostring(series_id) .."): no series found")
		return nil
	end

	local boxes_not_in = {}
	if series_id then
		-- Add boxes not in given series
		for box_id, _ in pairs(boxes) do
			local box_is_in = false
			local serie = series[series_id]
			for i2, box_id2 in pairs(serie.boxes) do
				if box_id == box_id2 then
					box_is_in = true
					break
				end
			end
			if not box_is_in then
				table.insert(boxes_not_in, box_id)
			end
		end
	else
		-- Add boxes not in any series
		for box_id, _ in pairs(boxes) do
			local box_is_in = false
			for series_id, serie in pairs(series) do
				for i2, box_id2 in pairs(serie.boxes) do
					if box_id == box_id2 then
						box_is_in = true
						break
					end
				end
			end
			if not box_is_in then
				table.insert(boxes_not_in, box_id)
			end
		end

	end
	return boxes_not_in
end

-- Required for compabiilty
function db.shutdown()
	-- Nothing needs to be done for the mod storage backend
end
