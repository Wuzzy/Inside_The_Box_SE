# textdomain: menu
Music is now disabled.=
Music is now enabled. Music playback will start later.=
Game=
Toggle music=
Box name=
Sky box:=
off=
Dark and Stormy=
Cloudy with light rays=
Full Moon=
Sunset=
Thick clouds over water=
Tropical sunny day=
Save this box=
Stop editing this box=
Wipe everything in this box=
Landscape this box=
Erasing the content is irreversible, and can not be undone. Are you sure you want to continue?=
If you are sure, then put in the code "@1" in the text box below and press "Erase Everything".=
Code=
Cancel=
Erase everything=
Drop nodes to the left from your inventory to create base layers for your box with these nodes. The stack size determines the thickness of the layers. Empty slots are omitted. Put torches in to leave multiple layers empty. The blocks in those layers will not be removed.=
Put the code "@1" in the box here:=
You are in box @1: "@2" by @3=
Leave this box=
