local S = minetest.get_translator("reserved_names")

local reserved_names_list = {}

reserved_names = {}

-- Adds a player name to the reserved names list
reserved_names.add = function(name)
	reserved_names_list[name] = true
end

-- Return true if the given player name is reserved
reserved_names.is_reserved = function(name)
	return reserved_names_list[name] == true
end

-- Auto-kick player if name is reserved
minetest.register_on_newplayer(function(player)
	local name = player:get_player_name()
	if reserved_names.is_reserved(name) then
		minetest.kick_player(name, S("This name is not allowed. Try another name."))
		minetest.remove_player(name)
	end
end)
