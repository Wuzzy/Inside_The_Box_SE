
--[[

  ITB (insidethebox) minetest game - Copyright (C) 2017-2018 sofar & nore

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public License
  as published by the Free Software Foundation; either version 2.1
  of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free
  Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
  MA 02111-1307 USA

]]--

-- disable all mapgens
minetest.set_mapgen_setting("mg_name", "singlenode", true)

local SP = {x=0, y=-3, z=0} -- start pos
local modpath = minetest.get_modpath("mg")

-- Place a simple spawn platform in spawn
minetest.register_on_generated(function(minp, maxp)
	if minp.x <= SP.x and minp.y <= SP.y and minp.z <= SP.z and maxp.x >= SP.x and maxp.y >= SP.y and maxp.z >= SP.z then
		local ret = minetest.place_schematic(
				SP,
				modpath .. "/schems/mg_simplespawn.mts",
				"0",
				{},
				false,
				"place_center_x, place_center_z")
		if ret == nil then
			minetest.log("error", "[mg] Spawn platform schematic could not be loaded!")
		elseif ret == false then
			minetest.log("error", "[mg] Spawn platform was only placed partially!")
		else
			minetest.log("action", "[mg] Spawn platform placed!")
		end
	end
end)

-- squelch mapgen warnings
minetest.register_alias("mapgen_stone", "nodes:stone")
minetest.register_alias("mapgen_water_source", "nodes:water_source")
minetest.register_alias("mapgen_river_water_source", "nodes:river_water_source")
