# `music` mod for Inside The Box

This mod adds the music framework for Inside the Box.
It does not contain music tracks themselves, those must
be added by a mod calling `music.set_tracks` (see code
commend for documentation).
