## Singleplayer TODO

### High priority
- Pick a new game name
- Build main islands
    * Play section
    * Editor section (with tutorial)
- Add loads of new, unique singleplayer levels
- Curate boxes and put them manually into themed series
    * Example: Forest, desert, arctic, ...
    * Example: Easy, Medium, Difficult?
    * The goal is to have boxes fit better together rather than being a large but unsorted collection of levels
- Add tutorial levels
    * But easier than the ITB server

### Maintenance
- Allow to replay boxes in sequential series
- Rip out perks
    * Allow free zooming because why not?

### Long-term goals
- Lore?
    * Revealing the secret of the nexus
    * Secret order of the nexus?
    * ???
- Make fully translatable
    * Including box contents like signs and terminals
        * Needs rewrite

### UI
- Rewrite terminals
    * Completely overhaul interface
    * Button-based interface, no more typing
    * Only show relevant commands you can actually use
    * Print message when something unlocks
    * Add commands to trigger mech nodes
- Get rid of ugly fallback formspec style

### Documentation
- Write instructions on how to import/export boxes
- WRite instructions on how to develop/maintain boxes

## Main game TODO

- fence and wall sounds are bugged due to high collision box

* lava bubble sound missing

- bad sounds:
   * glass footstep sound
   * gravel sounds 'wet'
   * snow footsteps: remove high freq?

- check placement code for all breakable nodes and make sure they can't be arbitrarily placed.
- block modifiers (anything that does `set_`, `remove_` or `swap_` \*node)

- light level in entry/exit lobbies is sometimes bugged at max light

- chat text color filtering is broken?

- tutorial: part chooser once completed

- spurious placeholders?

- terminal: move content to `mod_storage`

- terminal: make more useful (box/player stats?)

- better fix falling through lobby at box entry

- Spikey blocks?

- series maintenance GUI

- pick better understandable icons

- support for "patching" boxes - have other players submit newer versions for review?!

- build time in inventory

- teleports: modify attributes with a gui

- winter vegetation/grass/weed

- skybox-related ambiance sounds:
  - night: trunk -> owl
  - grass: windy -> wind sounds
           dark and stormy -> perhaps rain drop sounds?
  - water: sloshing lake wave?
  - sunny: tree leaves & cicadas

- swimming sounds

