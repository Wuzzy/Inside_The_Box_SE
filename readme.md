Inside The Box SE
=================

(SE = Singleplayer Edition)

A singleplayer puzzle solving and creation game for Luanti.
WARNING: There are NO LEVELS so far!

The basic concept of the game is boxes. This is an area in which the
player needs to find one or more objectives (Nexus). The player then
needs to bring these objectives to a slot (pedestal). Once all the
secrets have been found and returned, an exit opens that allows the
player out, completing the puzzle box.

This game is a derivate of Inside The Box, which is multiplayer.

## Relationship to Inside The Box (the original)

This game is a fork of Inside The Box with the goal to turn
this game into a true singleplayer game that can be played offline.

The goal is to create a fully self-contained game that also
has a healthy number of levels. Also, unlike Inside The Box,
the levels will be more themed and organized. In the original
Inside The Box, the levels are pretty much all over the place
and only very loosely organized.

ITBSE attempts to be compatible with boxes exported from
the original ITB.

## Playing

A player can choose to play available boxes. Selecting an available
level to play will create a private copy of the level box and teleport
the player to the starting point of the level. The starting point is
in a hallway or entry room. There can be some clues left, and there
should be some information of the builder, and perhaps difficulty,
time spent by players in the level, popularity, rating information.

From the hallway the player should have some visual on the inside of
the box. This will help players recognize boxes easier so they can
skip ones they have played before, or gauge difficulty, etc.

The player moves through a door to the level. This may trigger a
teleport to a starting location, or not. On the side of the wall
somewhere is displayed a large number corresponding with the box ID.

In the box there is a collection area. In the collection area there
are 1 or more collection slots. The slots all require to be filled
with a "key" item. These items are found anywhere in the level. The
player picks up the item and brings them to the collection slots.

The box largely contains unbreakable nodes. The box may contain certain
digging tools to break nodes in the box (e.g. an axe to break certain
wood blocks, a pickaxe to break stone blocks, or a shovel to dig
sand blocks).  The player may be forced to use these tools to reach
new areas in the box, or to create bridges or ladders to climb up.

The player can not physically exit the box. The box is surrounded by
unbreakable blocks uncluding invisible ceilings where needed. The
player has an "escape" button that, when used, removes the player
from the game and returns the player to the general lobby.

The player may take damage in the box through falling or lava or other
methods. The player recovers lost health somewhat quickly, but
some mistakes may be deadly.

Once the player collects all the needed items, an exit doorway opens
or becomes reachable. The player moves through the exit doorway and
ends in an exit lobby/hallway.

In the exit lobby/hallway, the player can proceed to the next level
Last, there is a method for the player to return to the general
lobby.


## Constructing

Players can construct new boxes. They do this by entering a pristine
box that is created on the fly for them. In this box, the player
receives full editing privileges. The player can construct and modify
the collection point, place the secrets, place any node or liquid,
fire, TNT or other element provided to them.

The entry hallway has a spot for a sign hint that the constructor can
modify. It also has an interface for modifying the box parameters,
such as name and size, and variations such as entry/exit gate locations
and collection point stations.


## Series

Series are curated boxes that can be played in succession. A
series of boxes grows one box at a time (interval to be
determined) and the newest box is always the top ranked box that is
not yet in that series. It may be that a fixed amount of scoring points
needs to be gained before a box can be promoted to a series.


## Roles

- Admin: reserved role for a player that can always moderate players
and/or boxes, ban/kick players, edit all boxes, remove boxes.
- Moderator: may suspend players, may suspend boxes.
- Player: no privileges.


## Technology

For creation and playing of maps, a virtual `plot` is reserved per
player in a random location on the map away from the static lobby. This
reservation is destroyed when the box is no longer needed.

The box contents are stored in the mod storage and restored for
each player that attempts to play the box. This assures that multiple
players can separately try the same box without conflicts.

The mod storage also allows storing and retrieval of
scores for players or boxes and creating ranks, modifying or inspecting
state of boxes or players.

Placement of boxes will use VoxelManips as well as additional metadata
where needed to connect dynamic elements inside the box. These are
subsequently connected to assure proper functioning of the box.


## Running this game

 1. Install this game in the `games` folder like a normal game.

 2. Configure your admin account by setting `name = adminname` in the
    `minetest.conf` setting for this world. The admin gets a creative
    inventory access while players do not. There is no need to run the
    world in `creative` mode.

After that, you should be able to log in and start building a lobby,
and use the commands to create and play boxes.

## Music

This game ships with music by The Cynic Project from the Pixelsphere
album. See `mods/music_tracks` for a track list.

The music is released under
CC0 <https://creativecommons.org/publicdomain/zero/1.0/>.

## World settings

Additional world settings you may need to change later are:


Optional world settings are:

```
lobby_respawn_center = (<x>,<y>,<z>) -- where to respawn players who come back
                                     -- to the lobby
lobby_respawn_radius = <min>,<max>   -- use this radius around the center

tutorial_required = <true,false>     -- are players required to complete this
tutorial_series = <id>               -- number of the tutorial series
tutorial_entry_lobby = <id>          -- tutorial entry lobby box_id
tutorial_exit_lobby = <id>           -- tutorial exit lobby box_id
tutorial_exit = (<x>,<y>,<z>)        -- where to teleport player at end of tutorial

announce_irc_admins = "john,frank"   -- no spaces, lists irc names that will receive admin announcements

```
